Source: gdk-pixbuf-xlib
Section: oldlibs
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Emilio Pozuelo Monfort <pochu@debian.org>, Iain Lane <laney@debian.org>, Laurent Bigonville <bigon@debian.org>
Build-Depends: debhelper-compat (= 13),
               libgdk-pixbuf-2.0-dev (>= 2.40.0),
               libglib2.0-dev (>= 2.48.0),
               libx11-dev,
               meson (>= 0.46.0)
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://www.gtk.org/
Vcs-Git: https://salsa.debian.org/gnome-team/gdk-pixbuf-xlib.git
Vcs-Browser: https://salsa.debian.org/gnome-team/gdk-pixbuf-xlib

Package: libgdk-pixbuf-xlib-2.0-0
Architecture: any
Depends: shared-mime-info,
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Recommends: libgdk-pixbuf2.0-bin
Breaks: libgdk-pixbuf2.0-0 (<< 2.40.0+dfsg-6~)
Replaces: libgdk-pixbuf2.0-0 (<< 2.40.0+dfsg-6~)
Multi-Arch: same
Description: GDK Pixbuf library (deprecated Xlib integration)
 This package contains a deprecated library to render GdkPixbuf structures
 to X drawables using Xlib (libX11).
 .
 No newly written code should ever use this library.
 .
 If your existing code depends on gdk-pixbuf-xlib, then you're strongly
 encouraged to port away from it.

Package: libgdk-pixbuf-xlib-2.0-dev
Architecture: any
Multi-Arch: same
Depends: libgdk-pixbuf-xlib-2.0-0 (= ${binary:Version}),
         libgdk-pixbuf-2.0-dev (>= 2.40.0),
         libglib2.0-dev (>= 2.38.0),
         libx11-dev,
         shared-mime-info,
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: libgdk-pixbuf2.0-dev (<< 2.40.0+dfsg-6~)
Replaces: libgdk-pixbuf2.0-dev (<< 2.40.0+dfsg-6~)
Description: GDK Pixbuf library (development files)
 This package contains the header files which are needed for using
 GDK Pixbuf's deprecated Xlib integration.
 .
 No newly written code should ever use this library.
 .
 If your existing code depends on gdk-pixbuf-xlib, then you're strongly
 encouraged to port away from it.
